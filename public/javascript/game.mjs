import {
  showMessageModal,
  showInputModal,
  showResultsModal,
} from './views/modal.mjs';
import {
  appendRoomElement,
  removeRoomElement,
  updateNumberOfUsersInRoom,
  appendLetterElements,
} from './views/room.mjs';
import {
  appendUserElement,
  removeUserElement,
  changeReadyStatus,
  setProgress,
} from './views/user.mjs';
import { addClass, removeClass, createElement } from './helpers/domHelper.mjs';

let currentRoom = null;
let text = null;
let nextLetter = 0;
let isGame = false;

const username = sessionStorage.getItem('username');
const roomsPage = document.getElementById('rooms-page');
const roomsContainer = document.querySelector('#rooms-wrapper');
const gamePage = document.getElementById('game-page');
const roomName = document.getElementById('room-name');
const usersWrapper = document.getElementById('users-wrapper');
const timer = document.getElementById('timer');
const textContainer = document.getElementById('text-container');
const gamesTitle = document.querySelector('#rooms-page .title');

const readyButton = document.getElementById('ready-btn');
const addRoomBtn = document.getElementById('add-room-btn');
const quitBtn = document.getElementById('quit-room-btn');
const gameTimerSeconds = document.getElementById('game-timer-seconds');
const gameTimer = document.getElementById('game-timer');

if (!username) {
  window.location.replace('/login');
}

const socket = io('http://localhost:3002', { query: { username } });

socket.on('EXISTING_USER', existingUser);
socket.on('EXISTING_ROOM', existingRoom);
socket.on('SHOW_ROOMS', updateRooms);
socket.on('ADD_ROOM', addRoom);
socket.on('UPDATE_ROOMS', updateRooms);
socket.on('JOIN_ROOM_DONE', joinRoom);
socket.on('LEAVE_ROOM', leaveRoom);
socket.on('REMOVE_ROOM', removeRoom);
socket.on('CHANGE_USER_STATUS', changeUserStatus);
socket.on('CHANGE_OWN_STATUS', changeOwnStatus);
socket.on('EVERYBODY_READY', startTimer);
socket.on('PROGRESS_CHANGED', onProgressChanged);
socket.on('FINISH', onFinish);
socket.on('GAMEOVER', onNoTimeLeft);
socket.on('CHECK_EVERYONE', checkEveryone);

function existingUser() {
  roomsPage.innerHTML = '';
  showMessageModal({
    message: 'This name already exists',
    onClose: () => {
      sessionStorage.removeItem('username');
      window.location.replace('/login');
    },
  });
}

function existingRoom() {
  showMessageModal({
    message: 'Room with this name already exists',
  });
}

function onJoin(event) {
  socket.emit('JOIN_ROOM', event.target.dataset.roomName);
}

function addRoom({ roomName, users, canJoin }) {
  if (canJoin) {
    appendRoomElement({
      name: roomName,
      numberOfUsers: users.length,
      onJoin,
    });
  }
}

function updateRooms(rooms) {
  roomsContainer.innerHTML = '';
  if (!document.querySelector('.yourName')) {
    const nameElement = createElement({ tagName: 'h2', className: 'yourName' });
    nameElement.innerText = `Hello, ${username}`;
    gamesTitle.before(nameElement);
  }
  rooms.forEach(({ roomName, users, canJoin }) => {
    if (canJoin) {
      appendRoomElement({
        name: roomName,
        numberOfUsers: users.length,
        onJoin,
      });
    }
  });
}

function joinRoom(room) {
  addClass(roomsPage, 'display-none');
  removeClass(gamePage, 'display-none');
  showCurrentRoom(room);
}

function showCurrentRoom(room) {
  currentRoom = room;
  roomName.innerHTML = room.roomName;
  usersWrapper.innerHTML = '';
  room.users.forEach(user => {
    const isCurrentUser = user.username === username;
    const ready = user.ready;
    appendUserElement({
      username: user.username,
      ready: ready,
      isCurrentUser,
    });
    if (isCurrentUser) readyButton.innerHTML = !ready ? 'READY' : 'NOT READY';
  });
}

function removeRoom(room) {
  removeRoomElement(room);
}

function leaveRoom(room, userName, userLength) {
  if (username === userName) {
    removeClass(roomsPage, 'display-none');
    addClass(gamePage, 'display-none');
    currentRoom = null;
    text = null;
    nextLetter = 0;
    isGame = false;
  }
  if (currentRoom?.roomName === room) {
    removeUserElement(userName);
  }
  if (!userLength || isGame) return;
  updateNumberOfUsersInRoom({ name: room, numberOfUsers: userLength });
}

function changeUserStatus(userName, status) {
  if (!currentRoom) return;
  changeReadyStatus({ username: userName, ready: status });
}

function changeOwnStatus(status) {
  readyButton.innerHTML = !status ? 'READY' : 'NOT READY';
}

async function startTimer(id, time, game_time) {
  isGame = true;
  const res = await fetch(`/game/texts/${id}`);
  text = await res.text();
  socket.emit('START_TIMER');
  addClass(quitBtn, 'display-none');
  addClass(readyButton, 'display-none');
  removeClass(timer, 'display-none');
  let timeLeft = time;
  timer.innerText = timeLeft;
  const intervalId = setInterval(() => {
    timeLeft -= 1;
    timer.innerText = timeLeft;
  }, 1000);
  setTimeout(() => {
    clearInterval(intervalId);
    addClass(timer, 'display-none');
    startGame(game_time);
  }, time * 1000);
}

function startGame(time) {
  removeClass(textContainer, 'display-none');
  appendLetterElements(text);
  const letter = document.querySelector('.normal');
  removeClass(letter, 'normal');
  removeClass(gameTimer, 'display-none');
  let timeLeft = time;
  gameTimerSeconds.innerText = timeLeft;
  document.addEventListener('keydown', onKeyPress);
  const intervalId = setInterval(() => {
    timeLeft -= 1;
    gameTimerSeconds.innerText = timeLeft;
  }, 1000);
  setTimeout(() => {
    clearInterval(intervalId);
  }, time * 1000);
}

// game process

function onKeyPress(event) {
  const { key } = event;
  const textLength = text.length;

  if (key === ' ') event.preventDefault();
  if (nextLetter === textLength) return;
  if (key !== text[nextLetter]) return;

  const letter = document.querySelector('.normal');
  const letterNext = document.querySelector('.next');

  if (nextLetter !== textLength - 1) removeClass(letter, 'normal');

  removeClass(letterNext, 'next');

  nextLetter = nextLetter + 1;

  const progress = (nextLetter / textLength) * 100;

  socket.emit('PROGRESS_CHANGE', currentRoom.roomName, username, progress);
}

function onProgressChanged(roomName, userName, progress) {
  if (currentRoom.roomName !== roomName) return;
  setProgress({ username: userName, progress });
  if (username === userName) {
    hasEverybodyFinished();
  }
}

function hasEverybodyFinished() {
  const progressArray = [...document.getElementsByClassName('user-progress')];
  if (!progressArray || progressArray.length === 0) return;

  if (progressArray.every(element => element.classList.contains('finished'))) {
    socket.emit('EVERYONE_FINISHED', currentRoom);
  }
}

function onFinish(room) {
  isGame = false;
  text = null;
  nextLetter = 0;

  readyButton.innerText = 'READY';
  updateNumberOfUsersInRoom({
    name: room.roomName,
    numberOfUsers: room.users.length,
  });

  const progressArray = [...document.getElementsByClassName('user-progress')];

  progressArray.forEach(element => {
    removeClass(element, 'finished');
  });

  const modals = document.getElementsByClassName('modal');
  if (modals.length === 1) return;
  document.removeEventListener('keydown', onKeyPress);
  addClass(textContainer, 'display-none');
  addClass(gameTimer, 'display-none');

  room.users.forEach(user => {
    setProgress({ username: user.username, progress: 0 });
    changeReadyStatus({ username: user.username, ready: false });
  });

  let sortedArray = room.users;
  sortedArray.sort((a, b) => (a.progress > b.progress ? -1 : 1));

  const usersSortedArray = sortedArray.map(({ username }) => username);

  showResultsModal({
    usersSortedArray,
    onClose: () => {
      removeClass(quitBtn, 'display-none');
      removeClass(readyButton, 'display-none');
    },
  });
}

function onNoTimeLeft() {
  socket.emit('EVERYONE_FINISHED', currentRoom);
}

function checkEveryone() {
  hasEverybodyFinished();
}

// eventListeners

addRoomBtn.addEventListener('click', onClickAddBtn);
quitBtn.addEventListener('click', onClickQuitBtn);
readyButton.addEventListener('click', onClickReadyBtn);

function onClickAddBtn() {
  let name;
  showInputModal({
    title: 'Create room',
    onChange: data => {
      name = data.trim();
    },
    onSubmit: () => {
      if (name.length === 0) return false;
      socket.emit('CREATE_ROOM', name);
      return true;
    },
  });
}

function onClickQuitBtn() {
  socket.emit('LEAVE_ROOM', currentRoom.roomName);
}

function onClickReadyBtn() {
  socket.emit('CHANGE_STATUS', username, currentRoom.roomName);
}
