import {
  isEverybodyReady,
  findRoom,
  getRandomNumber,
  findRoomByUser,
  removeUser,
  removeRoom,
  changeRoomStatus,
  setUserStatus,
  changeUserProgress,
} from './helper';
import * as config from './config';
import { texts } from '../data';

const users = new Set();
let rooms = [];
let timeoutId = null;

const getCurrentRoomId = socket =>
  Object.keys(socket.rooms).find(({ roomName }) => findRoom(rooms, roomName));

const addUser = (roomName, username) => {
  const room = findRoom(rooms, roomName);
  if (room.users.length + 1 > config.MAXIMUM_USERS_FOR_ONE_ROOM) return false;
  const canJoin = room.users.length + 1 !== config.MAXIMUM_USERS_FOR_ONE_ROOM;
  rooms = rooms.map(room =>
    room.roomName !== roomName
      ? room
      : {
          ...room,
          users: [...room.users, { username, ready: false }],
          canJoin,
        },
  );
  return true;
};

const changeUserStatus = (userName, roomName) => {
  let status = false;
  const changeUser = (user, userName) => {
    if (user.username !== userName) {
      return user;
    }
    status = !user.ready;
    return {
      ...user,
      ready: status,
    };
  };
  rooms = rooms.map(room => {
    return room.roomName !== roomName
      ? room
      : {
          ...room,
          users: room.users.map(user => {
            return changeUser(user, userName);
          }),
        };
  });
  return status;
};

export default io => {
  io.on('connection', socket => {
    const username = socket.handshake.query.username;
    if (users.has(username)) {
      return socket.emit('EXISTING_USER');
    } else {
      users.add(username);
    }

    socket.emit('SHOW_ROOMS', rooms);

    socket.on('CREATE_ROOM', roomName => {
      if (findRoom(rooms, roomName)) {
        return socket.emit('EXISTING_ROOM');
      }
      const prevRoomId = getCurrentRoomId(socket);
      if (roomName === prevRoomId) {
        return;
      }
      if (prevRoomId) {
        socket.leave(prevRoomId);
      }
      rooms.push({
        roomName,
        users: [{ username, ready: false, progress: 0 }],
        canJoin: true,
      });
      const room = findRoom(rooms, roomName);
      io.emit('ADD_ROOM', room);
      socket.join(roomName);
      io.to(socket.id).emit('JOIN_ROOM_DONE', room);
    });

    socket.on('JOIN_ROOM', roomName => {
      if (!findRoom(rooms, roomName)) {
        return;
      }

      const prevRoomId = getCurrentRoomId(socket);
      if (roomName === prevRoomId) {
        return;
      }

      if (prevRoomId) {
        socket.leave(prevRoomId);
        rooms = removeUser(prevRoomId, username, rooms);
      }

      const changed = addUser(roomName, username);
      if (!changed) return;
      const room = findRoom(rooms, roomName);
      socket.join(roomName);
      io.to(roomName).emit('JOIN_ROOM_DONE', room);
      if (isEverybodyReady(room)) {
        rooms = changeRoomStatus(roomName, false, rooms);
        io.to(roomName).emit(
          'EVERYBODY_READY',
          getRandomNumber(0, texts.length),
          config.SECONDS_TIMER_BEFORE_START_GAME,
          config.SECONDS_FOR_GAME,
        );
      }
      if (room.users.length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        rooms = changeRoomStatus(roomName, false, rooms);
      }
      io.emit('UPDATE_ROOMS', rooms);
    });

    socket.on('LEAVE_ROOM', roomName => {
      changeUserStatus(username, roomName);
      socket.leave(roomName);
      rooms = removeUser(roomName, username, rooms);
      const room = findRoom(rooms, roomName);
      const userLength = room.users.length;
      if (userLength === 0) {
        rooms = removeRoom(roomName, rooms);
        if (timeoutId) {
          clearTimeout(timeoutId);
          timeoutId = null;
        }
        io.emit('REMOVE_ROOM', roomName);
      }
      io.emit('LEAVE_ROOM', roomName, username, userLength);
      const updatedRoom = findRoom(rooms, roomName);
      if (updatedRoom && isEverybodyReady(updatedRoom)) {
        rooms = changeRoomStatus(roomName, false, rooms);
        io.to(roomName).emit(
          'EVERYBODY_READY',
          getRandomNumber(0, texts.length),
          config.SECONDS_TIMER_BEFORE_START_GAME,
          config.SECONDS_FOR_GAME,
        );
      }
      if (
        updatedRoom &&
        updatedRoom.users.length !== config.MAXIMUM_USERS_FOR_ONE_ROOM &&
        !isEverybodyReady(updatedRoom)
      ) {
        rooms = changeRoomStatus(updatedRoom.roomName, true, rooms);
      }
      io.emit('UPDATE_ROOMS', rooms);
    });

    socket.on('CHANGE_STATUS', (userName, roomName) => {
      const status = changeUserStatus(userName, roomName);
      io.emit('CHANGE_USER_STATUS', userName, status);
      socket.emit('CHANGE_OWN_STATUS', status);
      const room = findRoom(rooms, roomName);
      if (isEverybodyReady(room)) {
        rooms = changeRoomStatus(roomName, false, rooms);
        io.to(roomName).emit(
          'EVERYBODY_READY',
          getRandomNumber(0, texts.length),
          config.SECONDS_TIMER_BEFORE_START_GAME,
          config.SECONDS_FOR_GAME,
        );
      }
      if (
        !isEverybodyReady(room) &&
        room.users.length !== config.MAXIMUM_USERS_FOR_ONE_ROOM
      )
        rooms = changeRoomStatus(roomName, true, rooms);
      io.emit('UPDATE_ROOMS', rooms);
    });

    socket.on('PROGRESS_CHANGE', (roomName, username, progress) => {
      rooms = changeUserProgress(username, roomName, progress, rooms);
      io.to(roomName).emit('PROGRESS_CHANGED', roomName, username, progress);
    });

    socket.on('EVERYONE_FINISHED', room => {
      if (timeoutId) {
        clearTimeout(timeoutId);
        timeoutId = null;
      }
      room.users.forEach(user => {
        rooms = setUserStatus(user.username, room.roomName, false, rooms);
      });
      const newRoom = findRoom(rooms, room.roomName);
      room.users.forEach(user => {
        rooms = changeUserProgress(user.username, room.roomName, 0, rooms);
      });
      io.to(room.roomName).emit('FINISH', newRoom);
      if (newRoom.users.length !== config.MAXIMUM_USERS_FOR_ONE_ROOM) {
        rooms = changeRoomStatus(newRoom.roomName, true, rooms);
        io.emit('UPDATE_ROOMS', rooms);
      }
    });

    socket.on('START_TIMER', () => {
      timeoutId = setTimeout(() => {
        if (!timeoutId) return;
        socket.emit('GAMEOVER');
      }, (config.SECONDS_TIMER_BEFORE_START_GAME + config.SECONDS_FOR_GAME) * 1000);
    });

    socket.on('disconnect', () => {
      if (!users.has(username)) return;
      users.delete(username);
      const room = findRoomByUser(username, rooms);
      if (!room) return;
      const roomName = room.roomName;
      rooms = removeUser(roomName, username, rooms);
      const updatedRoom = findRoom(rooms, roomName);
      if (updatedRoom.users.length === 0) {
        rooms = removeRoom(roomName, rooms);
        if (timeoutId) {
          clearTimeout(timeoutId);
          timeoutId = null;
        }
        io.emit('REMOVE_ROOM', roomName);
      }
      io.emit('LEAVE_ROOM', roomName, username, updatedRoom.users.length);
      const newRoom = findRoom(rooms, roomName);
      if (newRoom && isEverybodyReady(newRoom) && !timeoutId) {
        rooms = changeRoomStatus(roomName, false, rooms);
        io.to(roomName).emit(
          'EVERYBODY_READY',
          getRandomNumber(0, texts.length),
          config.SECONDS_TIMER_BEFORE_START_GAME,
          config.SECONDS_FOR_GAME,
        );
      }
      if (newRoom) {
        io.to(roomName).emit('CHECK_EVERYONE');
        if (
          newRoom &&
          !isEverybodyReady(newRoom) &&
          newRoom.users.length !== config.MAXIMUM_USERS_FOR_ONE_ROOM
        ) {
          rooms = changeRoomStatus(newRoom.roomName, true, rooms);
        }
        io.emit('UPDATE_ROOMS', rooms);
      }
    });
  });
};
