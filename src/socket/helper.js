const isEverybodyReady = room => {
  if (!room || room?.users === 0) return;
  return room.users.every(({ ready }) => ready);
};

const findRoom = (rooms, name) => {
  return rooms.find(({ roomName }) => roomName === name);
};

const getRandomNumber = (min, max) => {
  return Math.floor(Math.random() * (max - min) + min);
};

const findRoomByUser = (username, rooms) => {
  for (const room of rooms) {
    for (const user of room.users) {
      if (user.username === username) {
        return room;
      }
    }
  }
  return null;
};

const removeUser = (rName, uName, rooms) => {
  const newRooms = rooms.map(room =>
    room.roomName !== rName
      ? room
      : { ...room, users: room.users.filter(user => user.username !== uName) },
  );
  return newRooms;
};

const removeRoom = (room, rooms) => {
  return rooms.filter(({ roomName }) => room !== roomName);
};

const changeRoomStatus = (roomName, status, rooms) => {
  return rooms.map(room => {
    return room.roomName !== roomName
      ? room
      : {
          ...room,
          users: room.users,
          canJoin: status,
        };
  });
};

const setUserStatus = (userName, roomName, status, rooms) => {
  const changeUser = (user, userName) => {
    if (user.username !== userName) {
      return user;
    }
    return {
      ...user,
      ready: status,
    };
  };
  return rooms.map(room => {
    return room.roomName !== roomName
      ? room
      : {
          ...room,
          users: room.users.map(user => {
            return changeUser(user, userName);
          }),
        };
  });
};

const changeUserProgress = (userName, roomName, progress, rooms) => {
  const changeUser = (user, userName, progress) => {
    if (user.username !== userName) {
      return user;
    }
    return {
      ...user,
      progress,
    };
  };
  return rooms.map(room => {
    return room.roomName !== roomName
      ? room
      : {
          ...room,
          users: room.users.map(user => {
            return changeUser(user, userName, progress);
          }),
        };
  });
};

export {
  isEverybodyReady,
  findRoom,
  getRandomNumber,
  findRoomByUser,
  removeUser,
  removeRoom,
  changeRoomStatus,
  setUserStatus,
  changeUserProgress,
};
